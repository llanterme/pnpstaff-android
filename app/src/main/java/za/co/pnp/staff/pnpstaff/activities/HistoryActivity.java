package za.co.pnp.staff.pnpstaff.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.pnp.staff.pnpstaff.R;
import za.co.pnp.staff.pnpstaff.adapters.HistoryAdapater;
import za.co.pnp.staff.pnpstaff.models.HistoryModel;
import za.co.pnp.staff.pnpstaff.models.UserModel;
import za.co.pnp.staff.pnpstaff.utils.ApiClient;
import za.co.pnp.staff.pnpstaff.utils.ApiInterface;
import za.co.pnp.staff.pnpstaff.utils.GeneralUtils;


public class HistoryActivity extends AppCompatActivity {

    ListView listView;
    ArrayList<HistoryModel> historyList;
    HistoryAdapater adapter;
    UserModel user;
    Call<ArrayList<HistoryModel>> call;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_history_list);

        setUpHistoryList();




    }

    public void setUpHistoryList() {

        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        user = GeneralUtils.getLoggedInUser(getWindow().getContext(),"USER");
        call = apiService.getistory(String.valueOf(user.getUserId()));

        historyList = new ArrayList<>();
        adapter = new HistoryAdapater(getBaseContext(), historyList);


        if (user == null) {
            GeneralUtils.showToastMessage("An error occured. Please check your connectivity!", HistoryActivity.this, "error");
        } else {

            call.enqueue(new Callback<ArrayList<HistoryModel>>() {

                @Override
                public void onResponse(Call<ArrayList<HistoryModel>> call, Response<ArrayList<HistoryModel>> response) {

                    if (response != null) {

                        for (HistoryModel rawHistory : response.body()) {
                            HistoryModel ahistory = new HistoryModel();
                            ahistory.setCount(rawHistory.getCount());
                            ahistory.setDateClaimed(rawHistory.getDateClaimed());
                            adapter.add(ahistory);
                        }
                    }
                }

                @Override
                public void onFailure(Call<ArrayList<HistoryModel>> call, Throwable t) {
                    GeneralUtils.showToastMessage("An error occured. Please check your connectivity!", HistoryActivity.this, "error");
                }
            });



        listView = (ListView) findViewById(R.id.historyList);
        listView.setAdapter(adapter);
        }



    }








}
