package za.co.pnp.staff.pnpstaff.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import za.co.pnp.staff.pnpstaff.R;
import za.co.pnp.staff.pnpstaff.models.StampsModel;
import za.co.pnp.staff.pnpstaff.models.UserModel;
import za.co.pnp.staff.pnpstaff.utils.ApiClient;
import za.co.pnp.staff.pnpstaff.utils.ApiInterface;
import za.co.pnp.staff.pnpstaff.utils.ConfigManager;
import za.co.pnp.staff.pnpstaff.utils.GeneralUtils;

public class MainActivity extends AppCompatActivity  {

    private static final String TAG = MainActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

        UserModel user = GeneralUtils.getLoggedInUser(getWindow().getContext(),"USER");

        if (user == null) {
            setupUser();
        }

        debug();


    }

    public void setupUser() {

        try {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            UserModel user = new UserModel(0, GeneralUtils.id(getWindow().getContext()));

            apiService.RegisterUser(user).enqueue(new Callback<UserModel>() {
                @Override
                public void onResponse(Call<UserModel> call, Response<UserModel> response) {

                    if (response.isSuccessful()) {

                        UserModel newUser = response.body();
                        Log.v(TAG, newUser.getDeviceId()); // Prints scan results

                        GeneralUtils.setPreferenceObject(getBaseContext(), newUser, "USER");

                    }
                }

                @Override
                public void onFailure(Call<UserModel> call, Throwable t) {
                    GeneralUtils.showToastMessage("An error occured. Please check your connectivity!", MainActivity.this, "error");

                }
            });

        } catch (Exception e)
        {
            Log.v(TAG, e.getMessage()); // Prints scan results
        }


    }


    public void debug() {



        StampsModel stamp =  new StampsModel();
        stamp.setStampCount(Integer.parseInt(ConfigManager.getPropValues("STAMPCOUNTDEBUG")));
        GeneralUtils.setPreferenceObject(getBaseContext(), stamp, "STAMPS");

    }

    public void toScannerActivity(View view) {

        Intent i = new Intent(this, ScannerActivity.class);
        startActivity(i);
    }

    public void toHistoryActivity(View view) {

        Intent i = new Intent(this, HistoryActivity.class);
        startActivity(i);
    }

    public void toStampsActivity(View view) {

        Intent i = new Intent(this, StampsActivity.class);
        startActivity(i);
    }


}
