package za.co.pnp.staff.pnpstaff.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.GridView;

import za.co.pnp.staff.pnpstaff.R;
import za.co.pnp.staff.pnpstaff.adapters.StampsAdapter;
import za.co.pnp.staff.pnpstaff.models.StampsModel;
import za.co.pnp.staff.pnpstaff.utils.GeneralUtils;

public class StampsActivity extends AppCompatActivity {

    private Menu menu;
    private  StampsModel localStamps;


    @Override
    public void onCreate(Bundle savedInstanceState) {




        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_stamps_grid);

        getSupportActionBar().show();
        setTitle("Your Stamps");


        localStamps = GeneralUtils.getLocalStampCount(getWindow().getContext(),"STAMPS");
        final StampsModel[] stamps = new StampsModel[localStamps.getStampCount()];

        GridView gridView = (GridView)findViewById(R.id.stampGridView);
        StampsAdapter booksAdapter = new StampsAdapter(this, stamps);
        gridView.setAdapter(booksAdapter);





    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_skip, menu);

        MenuItem claimButton = menu.findItem(R.id.claim_free_coffee);

        if(localStamps.getStampCount() == 8) {
            claimButton.setVisible(true);
        }

        return true;
    }
}
