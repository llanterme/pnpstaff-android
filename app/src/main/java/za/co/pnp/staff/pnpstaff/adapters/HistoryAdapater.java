package za.co.pnp.staff.pnpstaff.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import za.co.pnp.staff.pnpstaff.R;
import za.co.pnp.staff.pnpstaff.models.HistoryModel;

public class HistoryAdapater extends ArrayAdapter<HistoryModel> {

    public HistoryAdapater(Context context, ArrayList<HistoryModel> categories) {
        super(context, 0, categories);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        HistoryModel history = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_history_row, parent, false);
        }

        TextView month = (TextView) convertView.findViewById(R.id.historyMonth);
        month.setText(history.getDateClaimed());

        TextView count = (TextView) convertView.findViewById(R.id.historyCount);
        count.setText(String.valueOf(history.getCount()));


        return convertView;
    }

}


