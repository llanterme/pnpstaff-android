package za.co.pnp.staff.pnpstaff.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import java.util.ArrayList;

import za.co.pnp.staff.pnpstaff.R;
import za.co.pnp.staff.pnpstaff.models.StampsModel;

/**
 * Created by Luke on 4/17/17.
 */

public class StampAdap extends BaseAdapter {

    public class ViewHolder {

        ImageView stamp;

    }

    public ArrayList<StampsModel> stampsList;

    public Context context;

    private StampAdap(ArrayList<StampsModel> apps, Context context) {
        this.stampsList = apps;
        this.context = context;

    }

    @Override
    public int getCount() {
        return stampsList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        View rowView = convertView;
        ViewHolder viewHolder;

        if (rowView == null) {
            LayoutInflater inflater = LayoutInflater.from(context);;
            rowView = inflater.inflate(R.layout.gridview_stamps_row, parent,false);
            // configure view holder
            viewHolder = new ViewHolder();
            viewHolder.stamp = (ImageView) rowView.findViewById(R.id.coffee_bean_stamp);
            rowView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.stamp.setImageResource(R.drawable.bean);


        return rowView;


    }


}


