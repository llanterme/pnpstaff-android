package za.co.pnp.staff.pnpstaff.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import za.co.pnp.staff.pnpstaff.R;
import za.co.pnp.staff.pnpstaff.models.StampsModel;

import static za.co.pnp.staff.pnpstaff.R.drawable.bean;



public class StampsAdapter extends BaseAdapter {


    private final Context mContext;
    private final StampsModel[] stamps;

    // 1
    public StampsAdapter(Context context, StampsModel[] stamps) {
        this.mContext = context;
        this.stamps = stamps;
    }

    public View getView(int position, View convertView, ViewGroup parent) {


         if (convertView == null) {
            final LayoutInflater layoutInflater = LayoutInflater.from(mContext);
            convertView = layoutInflater.inflate(R.layout.gridview_stamps_row, null);
        }

        final ImageView imageView = (ImageView)convertView.findViewById(R.id.coffee_bean_stamp);
            imageView.setImageResource(R.drawable.bean);

        return convertView;
    }

    // 2
    @Override
    public int getCount() {
        return stamps.length;
    }

    // 3
    @Override
    public long getItemId(int position) {
        return 0;
    }

    // 4
    @Override
    public Object getItem(int position) {
        return null;
    }

}

