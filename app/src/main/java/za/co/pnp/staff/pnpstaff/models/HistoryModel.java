package za.co.pnp.staff.pnpstaff.models;

/**
 * Created by Luke on 4/16/17.
 */

public class HistoryModel {

    private int count;
    private String dateClaimed;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getDateClaimed() {
        return dateClaimed;
    }

    public void setDateClaimed(String dateClaimed) {
        this.dateClaimed = dateClaimed;
    }
}
