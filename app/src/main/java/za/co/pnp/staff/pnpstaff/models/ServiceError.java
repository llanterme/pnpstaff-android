package za.co.pnp.staff.pnpstaff.models;

/**
 * Created by Luke on 2/14/17.
 */

public class ServiceError {

    private String code;
    private String message;
    private String exceptionTrace;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getExceptionTrace() {
        return exceptionTrace;
    }

    public void setExceptionTrace(String exceptionTrace) {
        this.exceptionTrace = exceptionTrace;
    }
}
