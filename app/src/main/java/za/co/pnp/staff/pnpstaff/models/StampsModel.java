package za.co.pnp.staff.pnpstaff.models;



public class StampsModel {

    private int stampCount;

    private int gridCount;

    public int getStampCount() {
        return stampCount;
    }

    public void setStampCount(int stampCount) {
        this.stampCount = stampCount;
    }

    public int getGridCount() {
        return gridCount;
    }

    public void setGridCount(int gridCount) {
        this.gridCount = gridCount;
    }
}
