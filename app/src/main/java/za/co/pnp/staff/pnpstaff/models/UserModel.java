package za.co.pnp.staff.pnpstaff.models;

import java.io.Serializable;

/**
 * Created by Luke on 4/14/17.
 */

public class UserModel implements Serializable {

    private static final long serialVersionUID = 5531744639992436476L;


    private int userId;
    private String deviceId;

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public UserModel(int userId, String deviceId) {
        this.userId = userId;
        this.deviceId = deviceId;
    }
}
