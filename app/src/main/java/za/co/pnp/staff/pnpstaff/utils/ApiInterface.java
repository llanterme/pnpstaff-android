package za.co.pnp.staff.pnpstaff.utils;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import za.co.pnp.staff.pnpstaff.models.HistoryModel;
import za.co.pnp.staff.pnpstaff.models.UserModel;

public interface ApiInterface {

    @POST("user/add")
    Call<UserModel> RegisterUser(@Body UserModel user);

    @GET("loyalty/history")
    Call<ArrayList<HistoryModel>> getistory(@Query("userId") String userId);

}
