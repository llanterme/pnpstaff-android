package za.co.pnp.staff.pnpstaff.utils;


import android.content.Context;
import android.content.res.AssetManager;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigManager  {

    public static String getPropValues(String value) {

        Properties prop = new Properties();
        String propFileName = "config.properties";

        try {

            InputStream inputStream = ConfigManager.class.getClassLoader().getResourceAsStream(propFileName);

            prop.load(inputStream);
            if (inputStream == null) {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }
        } catch (IOException e) {

        }

        return prop.getProperty(value);
    }


        public  static String getProperty(String key, Context context) {

            try {
                Properties properties = new Properties();
                AssetManager assetManager = context.getAssets();
                InputStream inputStream = assetManager.open("config.properties");
                properties.load(inputStream);
                return properties.getProperty(key);
            }catch (Exception e) {
                return null;
            }

        }
    }


