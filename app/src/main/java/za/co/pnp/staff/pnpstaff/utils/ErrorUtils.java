package za.co.pnp.staff.pnpstaff.utils;

import java.io.IOException;
import java.lang.annotation.Annotation;

import okhttp3.ResponseBody;
import retrofit2.Converter;
import retrofit2.Response;
import za.co.pnp.staff.pnpstaff.models.ServiceError;

/**
 * Created by Luke on 2/14/17.
 */

public class ErrorUtils {

    public static ServiceError parseError(Response<?> response) {
        Converter<ResponseBody, ServiceError> converter =
                ApiClient.getClient().responseBodyConverter(ServiceError.class, new Annotation[0]);

        ServiceError error;

        try {
            error = converter.convert(response.errorBody());
        } catch (IOException e) {
            return new ServiceError();
        }

        return error;
    }
}
