package za.co.pnp.staff.pnpstaff.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.gson.Gson;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import java.util.UUID;

import za.co.pnp.staff.pnpstaff.models.StampsModel;
import za.co.pnp.staff.pnpstaff.models.UserModel;

/**
 * Created by Luke on 3/16/17.
 */

public class GeneralUtils {

    static public void setPreferenceObject(Context c, Object modal,String key) {
        /**** storing object in preferences  ****/
        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                c.getApplicationContext());
        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();

        Gson gson = new Gson();
        String jsonObject = gson.toJson(modal);
        prefsEditor.putString(key, jsonObject);
        prefsEditor.commit();

    }

    static public UserModel getLoggedInUser(Context c, String key) {

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                c.getApplicationContext());

        /**** get user data    *****/
        String json = appSharedPrefs.getString(key, "");
        Gson gson=new Gson();
        UserModel selectedUser= gson.fromJson(json, UserModel.class);
        return  selectedUser;
    }

    static public StampsModel getLocalStampCount(Context c, String key) {

        SharedPreferences appSharedPrefs = PreferenceManager.getDefaultSharedPreferences(
                c.getApplicationContext());

        /**** get user data    *****/
        String json = appSharedPrefs.getString(key, "");
        Gson gson=new Gson();
        StampsModel localStamps= gson.fromJson(json, StampsModel.class);
        return  localStamps;
    }


    public synchronized static String id(Context context) {

          String uniqueID = null;
          final String PREF_UNIQUE_ID = "PREF_UNIQUE_ID";
        if (uniqueID == null) {
            SharedPreferences sharedPrefs = context.getSharedPreferences(
                    PREF_UNIQUE_ID, Context.MODE_PRIVATE);
            uniqueID = sharedPrefs.getString(PREF_UNIQUE_ID, null);

            if (uniqueID == null) {
                uniqueID = UUID.randomUUID().toString();
                SharedPreferences.Editor editor = sharedPrefs.edit();
                editor.putString(PREF_UNIQUE_ID, uniqueID);
                editor.commit();
            }
        }

        return uniqueID;
    }

    static public void showToastMessage(String message, AppCompatActivity activity, String type) {

        StyleableToast st = new StyleableToast(activity, message, Toast.LENGTH_LONG);

        if(type == "error") {
            st.setBackgroundColor(Color.parseColor("#c70039"));
        }  else {
            st.setBackgroundColor(Color.parseColor("#2E8DFB"));
        }
        st.setBoldText();
        st.setTextColor(Color.WHITE);
        st.setCornerRadius(7);
        st.show();

    }

    static public void showToastMessage(String message, FragmentActivity activity, String type) {

        StyleableToast st = new StyleableToast(activity, message, Toast.LENGTH_LONG);
        if(type == "error") {
            st.setBackgroundColor(Color.parseColor("#c70039"));
        }  else {
            st.setBackgroundColor(Color.parseColor("#2E8DFB"));
        }
        st.setBoldText();
        st.setTextColor(Color.WHITE);
        st.setCornerRadius(7);


        st.show();

    }
}
